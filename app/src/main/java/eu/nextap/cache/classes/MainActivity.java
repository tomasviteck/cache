package eu.nextap.cache.classes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 * Class where developer set primary options and starting webView,
 *
 * @author Tomáš Vítek
 */

public class MainActivity extends AppCompatActivity {

    /**
     * Primary testing web url.
     */
    //http://www.vitraconnect.com/account/1/playlist/8/index.html OK
    //http://db.weshop.vision/playlists/DEMO/index.html OK
    //http://www.vitraconnect.com/account/1/playlist/64/index.html OK
    private static final String PAGE_URL = "http://www.vitraconnect.com/account/1/playlist/64/index.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Create new webView.
        WebView webview = new WebView(this);

        //Enable JavaScript.
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //We need to set WebChromeClient to play video in webView automatically
        webview.setWebChromeClient(new WebChromeClient());
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);

        //Set custom webViewClient.
        WebViewClientImpl webViewClient = new WebViewClientImpl(MainActivity.this, PAGE_URL);
        webview.setWebViewClient(webViewClient);

        //Load Url into webView.
        webview.loadUrl(PAGE_URL);

        //Show webView.
        setContentView(webview);
    }
}