package eu.nextap.cache.classes;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import eu.nextap.webcachelibrary.classes.UrlCache;
import eu.nextap.webcachelibrary.enums.CacheStorage;

/**
 * Client for webView specification,
 *
 * @author Tomáš Vítek
 */

public class WebViewClientImpl extends WebViewClient {

    /**
     * Declare objects.
     */
    private Activity mActivity;
    private UrlCache mUrlCache;
    private String mBaseUrl;

    /**
     * Create constructor.
     */
    public WebViewClientImpl(Activity activity, String baseUrl) {

        //Define Objects.
        this.mActivity = activity;
        this.mBaseUrl = baseUrl;
        this.mUrlCache = new UrlCache(activity.getApplicationContext());

        //Out own UrlCache settings.
        mUrlCache.setmCacheStorage(CacheStorage.INTERNAL_STORAGE);
        mUrlCache.setmMaxAgeMillis(15*UrlCache.ONE_MINUTE);
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return shouldOverride(url);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return shouldOverride(request.getUrl().toString());
    }

    Boolean shouldOverride(String url) {

        //Don't open web browser app.
        if (url.contains(mBaseUrl)) return false;

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(intent);
        return true;
    }

    @SuppressWarnings("deprecation")
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        return interceptRequest(url);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        return interceptRequest(request.getUrl().toString());
    }

    WebResourceResponse interceptRequest(String url) {

        //Return downloaded and cached resource.
        return this.mUrlCache.load(url);
    }
}