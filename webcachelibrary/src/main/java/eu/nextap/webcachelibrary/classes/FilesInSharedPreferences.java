package eu.nextap.webcachelibrary.classes;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Collections;

import eu.nextap.webcachelibrary.interfaces.CacheStateStories;

/**
 * This class will save and load back arrayList of files which are downloading to shared preferences,
 *
 * @author Tomáš Vítek
 */

public final class FilesInSharedPreferences implements CacheStateStories {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private final String FILES_KEY = "files";

    public FilesInSharedPreferences(Context context) {

        //Define sharedprefs and editor
        mSharedPreferences = context.getSharedPreferences(getClass().getSimpleName(), Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    @Override
    public void saveFilesInProgress(ArrayList<String> arrayList) {

        //Convert arrayList to string.
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arrayList.size(); i++) {
            stringBuilder.append(arrayList.get(i)).append(",");
        }

        //Save that string to shared preferences.
        mEditor.putString(FILES_KEY, String.valueOf(stringBuilder));
        mEditor.apply();
    }

    @Override
    public ArrayList<String> getFilesInProgress() {

        //Get string from shared preferences.
        String filesString = mSharedPreferences.getString(FILES_KEY, "");

        //Convert string to arrayList.
        String[] filesArray = filesString.split(",");
        ArrayList<String> filesArraylist = new ArrayList<>();
        Collections.addAll(filesArraylist, filesArray);

        //Return arrayList.
        return filesArraylist;
    }
}
