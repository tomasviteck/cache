package eu.nextap.webcachelibrary.classes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.util.Log;
import android.webkit.WebResourceResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import eu.nextap.webcachelibrary.enums.CacheEntryBehaviour;
import eu.nextap.webcachelibrary.enums.CacheEntryProcess;
import eu.nextap.webcachelibrary.enums.CacheStorage;
import eu.nextap.webcachelibrary.interfaces.CacheStateStories;
import eu.nextap.webcachelibrary.interfaces.PathGenerator;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static eu.nextap.webcachelibrary.enums.CacheEntryBehaviour.*;
import static eu.nextap.webcachelibrary.enums.CacheEntryProcess.*;
import static eu.nextap.webcachelibrary.enums.CacheStorage.*;

/**
 * All processes of caching like saving, loading or editing,
 *
 * @author Tomáš Vítek
 */

public final class UrlCache {

    /**
     * Define time constants how long will be cache saved.
     */
    public static final long ONE_SECOND = 1000L;
    public static final long ONE_MINUTE = 60L * ONE_SECOND;
    public static final long ONE_HOUR = 60L * ONE_MINUTE;
    public static final long ONE_DAY = 24 * ONE_HOUR;
    public static final long ONE_WEEK = 7 * ONE_DAY;
    public static final long ONE_MONTH = 4 * ONE_WEEK;
    public static final long ONE_YEAR = 12 * ONE_MONTH;
    public static final long INFINITY = 100000000 * ONE_YEAR;
    public static final String ENCODING = "UTF-8";

    /**
     * Class name.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * Define other variables.
     */
    private CacheEntryBehaviour mCacheEntryBehaviour;
    private CacheStorage mCacheStorage;
    private long mMaxAgeMillis;

    /**
     * These variables will we use to detect not downloaded file
     */
    private ArrayList<String> mDownloadingFilesArrayList = new ArrayList<>();

    /**
     * Define objects.
     */
    private Map<String, CacheEntry> mCacheEntries = new HashMap<String, CacheEntry>();
    private Context mContext;
    private File mRootDir;
    private PathGenerator mPathGenerator;
    private CacheStateStories mCacheStateStories;

    /**
     * Set thread to load web and in background download files.
     */
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private ThreadPoolExecutor mExecutor = new ThreadPoolExecutor(2 * NUMBER_OF_CORES, 2 * NUMBER_OF_CORES, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    /**
     * UrlCache constructor where we set default settings
     */
    public UrlCache(Context context) {
        this.mContext = context;

        //Set default settings
        setmMaxAgeMillis(ONE_HOUR);
        setmCacheEntryBehaviour(CACHE_ALL);
        setmCacheStorage(EXTERNAL_STORAGE);
        setFilesDirectory(getmCacheStorage());
        setmPathGenerator(new UrlPathGenerator());
        setmCacheStateStories(new FilesInSharedPreferences(context));
    }


    /**
     * Register new file in cache so create new path and generate cacheEntry.
     */
    public void registerCacheEntry(String url, String mimeType, String encoding) {

        //Create unique path.
        String filePath = this.mRootDir.getPath() + File.separator + mPathGenerator.createPath(url);

        //Create new CacheEntry.
        CacheEntry entry = new CacheEntry(url, filePath, mimeType, encoding);

        //Put url and CacheEntry into CacheEntries Map.
        this.mCacheEntries.put(url, entry);
    }


    /**
     * Load saved cache file or if cache file isn't available download and save it again.
     */
    public WebResourceResponse load(final String url) {

        //Get current CacheEntry.
        CacheEntry cacheEntry = this.mCacheEntries.get(url);

        //Check if cacheEntry isn't null.
        if (cacheEntry == null) {

            switch (getmCacheEntryBehaviour()) {

                case CACHE_ALL:

                    //Create new cacheEntry.
                    registerCacheEntry(url, FileUtils.getMimeType(url), ENCODING);
                    cacheEntry = this.mCacheEntries.get(url);
                    break;
                case USE_CACHE_ENTRY:
                    return null;
                default:
                    throw new IllegalArgumentException("Unknown CacheEntryBehaviour");
            }

        }

        //Find file in definite path.
        File cachedFile = new File(cacheEntry.filePath);

        //Check if cachedFile doesn't exists.
        if (cachedFile.exists()) {

            //Define new cacheEntry age.
            long cacheEntryAge = System.currentTimeMillis() - cachedFile.lastModified();

            //Check if new cacheEntry age isn't bigger than cacheEntry's max age.
            if (cacheEntryAge > mMaxAgeMillis) {

                //CacheEntry is not downloaded because time expires
                cacheEntry.setCacheEntryProcess(EXPIRED);

                //Cached file deleted.
                cachedFile.delete();

                //Call load() again.
                return load(url);
            }

            //If we detect any file started downloading but hasn't finished it we have to download file again.
            if (mCacheStateStories.getFilesInProgress().contains(cacheEntry.getFilePath()) && cacheEntry.getCacheEntryProcess() != DOWNLOADING) {

                Log.e(TAG, "Error file: " + cacheEntry.getFilePath());

                //CacheEntry is not downloaded because time expires
                cacheEntry.setCacheEntryProcess(ERROR);

                //Cached file deleted.
                cachedFile.delete();

                //Call load() again.
                return load(url);
            }

            //Cached file exists and is not too old so try to return File.
            try {
                return new WebResourceResponse(cacheEntry.getMimeType(), cacheEntry.getEncoding(), new FileInputStream(cachedFile));
            } catch (FileNotFoundException e) {
                cacheEntry.setCacheEntryProcess(ERROR);
                Log.e(TAG, e.getMessage(), e);
            }

        } else {

            //If internet isn't available we are not able to download files
            if (isInternetOn()) {

                //Start downloading files
                cacheEntry.setCacheEntryProcess(SCHEDULED);
                mExecutor.execute(new DownloadInBackground(url, cacheEntry));
            } else {
                cacheEntry.setCacheEntryProcess(ERROR);
                return null;
            }

        }
        return null;
    }

    /**
     * Download current file and save it into cache.
     */
    private void downloadAndStore(String url, CacheEntry cacheEntry) throws IOException {

        Log.d(TAG, "List of files in progress " + mDownloadingFilesArrayList.toString());
        Log.d(TAG, "Downloading " + url);

        cacheEntry.setCacheEntryProcess(DOWNLOADING);

        //Add new downloading file to arrayList of downloading files.
        mDownloadingFilesArrayList.add(cacheEntry.getFilePath());
        mCacheStateStories.saveFilesInProgress(mDownloadingFilesArrayList);

        //Define objects.
        URL urlObj = new URL(url);
        URLConnection urlConnection = urlObj.openConnection();

        //Create new file from cacheEntry path.
        File file = new File(cacheEntry.filePath);

        //Configure path.
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }

        //Open cached file.
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        InputStream urlInput = urlConnection.getInputStream();

        //Download current file
        FileUtils.copyAndSaveStreams(fileOutputStream, urlInput);

        //Close input and output streams.
        urlInput.close();
        fileOutputStream.close();

        //File was successful downloaded we can remove it from arrayList of downloading files.
        mDownloadingFilesArrayList = mCacheStateStories.getFilesInProgress();
        mDownloadingFilesArrayList.remove(cacheEntry.getFilePath());
        mCacheStateStories.saveFilesInProgress(mDownloadingFilesArrayList);

        cacheEntry.setCacheEntryProcess(DOWNLOADED);

        Log.d(TAG, "List: " + mDownloadingFilesArrayList.toString());
        Log.d(TAG, "Saved " + url);
    }

    /**
     * Will check if phone is connected to internet
     */
    private boolean isInternetOn() {

        //Get Connectivity Manager object to check connection.
        ConnectivityManager connec = (ConnectivityManager) mContext.getSystemService(CONNECTIVITY_SERVICE);

        //Check for network connections.
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
            return true;
        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }

    /**
     * Will set where will be files saved.
     */
    private void setFilesDirectory(CacheStorage cacheStorage) {
        switch (cacheStorage) {
            case EXTERNAL_STORAGE:
                this.mRootDir = Environment.getExternalStorageDirectory();
                break;
            case INTERNAL_STORAGE:
                this.mRootDir = this.mContext.getFilesDir();
                break;
        }
    }

    /**
     * Will remove root directory where are saved all files
     */
    public void removeRootDirectory(String url) {
        this.mPathGenerator.removeRootDirectory(url);
    }

    /**
     * Getters and setters.
     */

    public void setmCacheEntryBehaviour(CacheEntryBehaviour mCacheEntryBehaviour) {
        this.mCacheEntryBehaviour = mCacheEntryBehaviour;
    }

    public CacheEntryBehaviour getmCacheEntryBehaviour() {
        return mCacheEntryBehaviour;
    }

    public CacheStorage getmCacheStorage() {
        return mCacheStorage;
    }

    public void setmCacheStorage(CacheStorage mCacheStorage) {
        this.mCacheStorage = mCacheStorage;
    }

    public PathGenerator getmPathGenerator() {
        return mPathGenerator;
    }

    public void setmPathGenerator(PathGenerator mPathGenerator) {
        this.mPathGenerator = mPathGenerator;
    }

    public long getmMaxAgeMillis() {
        return mMaxAgeMillis;
    }

    public void setmMaxAgeMillis(long mMaxAgeMillis) {
        this.mMaxAgeMillis = mMaxAgeMillis;
    }

    public CacheStateStories getmCacheStateStories() {
        return mCacheStateStories;
    }

    public void setmCacheStateStories(CacheStateStories mCacheStateStories) {
        this.mCacheStateStories = mCacheStateStories;
    }

    /**
     * Will download files in background during web will be showed.
     */
    private class DownloadInBackground implements Runnable {

        private String url;
        private CacheEntry finalCacheEntry;

        DownloadInBackground(String url, CacheEntry cacheEntry) {
            this.url = url;
            this.finalCacheEntry = cacheEntry;
        }

        @Override
        public void run() {

            if (isInternetOn()) {

                //Download and save into cache.
                try {
                    downloadAndStore(url, finalCacheEntry);
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }
    }

    /**
     * CacheEntry is used to save one file from web, so we will use probably many mCacheEntries.
     */
    private static class CacheEntry {

        /**
         * Declare variables.
         */
        private String url;
        private String filePath;
        private String mimeType;
        private String encoding;
        private CacheEntryProcess cacheEntryProcess;

        /**
         * CacheEntry constructor.
         */
        private CacheEntry(String url, String filePath, String mimeType, String encoding) {

            //Define variables.
            this.url = url;
            this.filePath = filePath;
            this.mimeType = mimeType;
            this.encoding = encoding;

            this.cacheEntryProcess = CREATED;

        }

        public CacheEntryProcess getCacheEntryProcess() {
            return cacheEntryProcess;
        }

        public void setCacheEntryProcess(CacheEntryProcess cacheEntryProcess) {
            this.cacheEntryProcess = cacheEntryProcess;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public String getMimeType() {
            return mimeType;
        }

        public void setMimeType(String mimeType) {
            this.mimeType = mimeType;
        }

        public String getEncoding() {
            return encoding;
        }

        public void setEncoding(String encoding) {
            this.encoding = encoding;
        }
    }
}