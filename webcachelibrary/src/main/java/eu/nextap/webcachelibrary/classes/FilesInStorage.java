package eu.nextap.webcachelibrary.classes;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

import eu.nextap.webcachelibrary.interfaces.CacheStateStories;

/**
 * This classes will define methods which will save data on Internal Storage,
 *
 * @author Tomáš Vítek
 */

public final class FilesInStorage implements CacheStateStories {

    private Context mContext;
    private final String FILE_NAME = "filesArrayList.txt";
    private final String TAG = getClass().getSimpleName();

    public FilesInStorage(Context context) {
        this.mContext = context;
    }

    @Override
    public void saveFilesInProgress(ArrayList<String> arrayList) {
        StringBuilder stringBuilder = new StringBuilder();

        //Convert ArrayList of strings to one string.
        for (int i = 0; i < arrayList.size(); i++) {
            stringBuilder.append(arrayList.get(i)).append(",");
        }
        try {
            //Try to save this string on internal storage.
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput(FILE_NAME, Context.MODE_PRIVATE));
            outputStreamWriter.write(String.valueOf(stringBuilder));
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString());
        }
    }

    @Override
    public ArrayList<String> getFilesInProgress() {

        String stringOfArray = "";

        try {

            //Try to get string from storage.
            InputStream inputStream = mContext.openFileInput(FILE_NAME);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                stringOfArray = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e(TAG, "Can not read file: " + e.toString());
        }

        //Convert string to ArrayList.
        String[] filesArray = stringOfArray.split(",");
        ArrayList<String> filesArraylist = new ArrayList<>();
        Collections.addAll(filesArraylist, filesArray);

        //Return arraylist which has data from string from storage.
        return filesArraylist;
    }
}
