package eu.nextap.webcachelibrary.classes;

import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Utils contains static methods which do something in storage,
 *
 * @author Tomáš Vítek
 */

public abstract class FileUtils {

    /**
     * Wll return size of full web cache.
     */
    public static long getFileSize(File file) {

        //Default size value.
        long length = 0;

        //Check if file exists.
        if (file.exists()) {

            //Check if File is directory.
            if (file.isDirectory()) {

                //Get size of all files.
                for (File newFile : file.listFiles()) {

                    if (newFile.isFile()) {
                        length += newFile.length();
                    } else {
                        length += getFileSize(newFile);
                    }
                }
                return length;
            } else {
                //If file is File return its size.
                return file.length();
            }
        }
        return length;
    }

    /**
     * Will rewrite data in storage
     */
    public static void copyAndSaveStreams(FileOutputStream fileOutputStream, InputStream urlInput) throws IOException {

        //Read bytes of urlInput.
        int data = urlInput.read();

        //Check if lenght of bytes is not -1.
        while (data != -1) {

            //Save data.
            fileOutputStream.write(data);

            //Read bytes of urlInput again.
            data = urlInput.read();
        }
    }

    /**
     * Will return sorted list of files sorted by date of last.
     */
    public static List<File> listFilesSortedByModificationDate(File file) {

        //Create new List.
        List<File> filesList = new ArrayList<>();

        //Check if file exists and if is directory.
        if (file.exists() && file.isDirectory()) {

            //Define File list.
            File[] files = file.listFiles();

            //Sort by last modified date.
            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });

            //Save sorted List.
            Collections.addAll(filesList, files);
            return filesList;
        }
        return null;
    }

    /**
     * Will reduce directory size to max available size.
     */
    public static void reduceDirectoryToMaxSize(File file, double maxSize) {

        //Get files list from directory.
        List<File> filesList = listFilesSortedByModificationDate(file);

        //Check if filesList is not null.
        if (filesList != null) {

            //Remove files until directory size is smaller than max size.
            for (int i = 0; i < filesList.size(); i++) {
                if (getFileSize(file) > maxSize) {
                    filesList.get(i).delete();
                }
            }
        }
    }

    /**
     * Will return mimetype of web file.
     */
    public static String getMimeType(String url) {
        String type = "";

        if (url.lastIndexOf(".") != -1) {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(ext);
        }
        if (type == null) {
            return "";
        } else return type;
    }

}
