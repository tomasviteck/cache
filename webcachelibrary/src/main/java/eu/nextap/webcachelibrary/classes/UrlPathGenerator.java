package eu.nextap.webcachelibrary.classes;

import java.io.File;

import eu.nextap.webcachelibrary.interfaces.PathGenerator;

/**
 * Will create new directory or will remove it,
 *
 * @author Tomáš Vítek
 */

public final class UrlPathGenerator implements PathGenerator {

    private static final String SEPARATOR = "/";
    private static final int POSITION_WHERE_WEB_NAME_STARTS = 3, MINIMAL_URL_LENGHT = 4;

    @Override
    public String createPath(String url) {

        //Separate string from url.
        String[] separated = url.split(SEPARATOR);

        //Create path from splitted url.
        String path = "";
        for (int i = POSITION_WHERE_WEB_NAME_STARTS; i < separated.length; i++) {

            //Add current name to path.
            path += separated[i];

            //If name isnt last we can add separator.
            if (i != separated.length - 1) {
                path += File.separator;
            }
        }

        if (separated.length < MINIMAL_URL_LENGHT) {

            //Too short... we cant create path.
            return null;
        } else {
            //Return created path.
            return path;
        }
    }

    @Override
    public void removeRootDirectory(String url) {

        //Split url.
        String[] separated = url.split("/");

        //Check if root exists.
        if (separated.length < MINIMAL_URL_LENGHT) {
        } else {
            //Get root file.
            File file = new File(separated[POSITION_WHERE_WEB_NAME_STARTS]);

            //Delete it.
            file.delete();
        }
    }
}
