package eu.nextap.webcachelibrary.enums;

/**
 * Enum for options to save cache on external or internal storage,
 *
 * @author Tomáš Vítek
 */

public enum CacheStorage {

    /**
     * EXTERNAL_STORAGE will be used when we want to save cache files on external storage.
     * INTERNAL_STORAGE will be used when we want to save files on internal storage.
     */
    EXTERNAL_STORAGE, INTERNAL_STORAGE
}
