package eu.nextap.webcachelibrary.enums;

/**
 * Enum for options when developer wants to save all cache entries or only one,
 *
 * @author Tomáš Vítek
 */

public enum CacheEntryBehaviour {

    /**
     * USE_CACHE_ENTRY will be used when we want to save simple web pages and
     * CACHE_ALL when we want to save all the files of web.
     */
    USE_CACHE_ENTRY, CACHE_ALL
}
