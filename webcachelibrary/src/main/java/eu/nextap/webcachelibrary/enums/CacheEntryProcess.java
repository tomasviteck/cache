package eu.nextap.webcachelibrary.enums;

/**
 * These enums will be used to specify current cache entry process,
 *
 * @author Tomáš Vítek
 */

public enum CacheEntryProcess {

    /**
     * CREATED will be used when cache entry si created.
     * SCHEDULED will be used when cache entry is ready to download but is not downloading.
     * DOWNLOADING will be used when cache entry is already downloading.
     * DOWNLOADED will be used when cache entry was successfully downloaded and saved.
     * ERROR will be used when cache entry was downloaded wrongly.
     * EXPIRED will be used when cache entry is over max age.
     */
    CREATED, SCHEDULED, DOWNLOADING, DOWNLOADED, ERROR, EXPIRED

}
