package eu.nextap.webcachelibrary.interfaces;

import java.util.ArrayList;

/**
 * These methods are able to save and get array of files in progress,
 *
 * @author Tomáš Vítek
 */

public interface CacheStateStories {

    /**
     * This method will save arrayList of files to storage.
     */
    void saveFilesInProgress(ArrayList<String> arrayList);

    /**
     * Will return ArrayList from storage
     */
    ArrayList<String> getFilesInProgress();

}
