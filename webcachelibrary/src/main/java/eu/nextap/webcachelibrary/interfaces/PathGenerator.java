package eu.nextap.webcachelibrary.interfaces;

/**
 * Interfaces will add methods which will create new unique path for directory like same as url page and
 * removeRootDirectory method will remove root directory what means will remove cache for whole website,
 *
 * @author Tomáš Vítek
 */

public interface PathGenerator {

    /**
     * Will create unique path for cached file, path will looks like website path for current file.
     */
    String createPath(String url);

    /**
     * Will remove full directory for whole web caches.
     */
    void removeRootDirectory(String url);
}
